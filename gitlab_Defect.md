Number  Type        Description                                                             Introduce During    Removed During
1.      Syntax      Forget to add throw Exception for main function                         Development         Testing
2.      Function    Misuse of &&, || in function phase                                      Development         Testing
3.      Assignment  Try to use ArraytoList in a mutable size list                           Development         Testing
4.      Syntax      Use System.out.print instead of System.out.println for several times    Development         Testing
5.      Syntax      Use System.out.println instead of System.err.println                    Development         Testing
6.      Interface   Read in, split first line without sanitize and check for repeatation    Planning            Testing
7.      Function    Forget to deal with word which are empty after sanitize                 Development         Testing
8.      Functuion   Misuse of &&, ! in function stop_or_not and contains                    Development         Testing
9.      Syntax      Forget [] access is unmutable, forget to use word_count.set()           Development         Development
10.     Interface   Forget to close input file                                              Development         Development 